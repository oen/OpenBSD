# Change to veb(4) interface in OpenBSD 7.1

I couple of weeks ago I saw a [discussion on reddit](https://www.reddit.com/r/openbsd/comments/rn30n0/vlans_vebs_vports_having_issues_trying_to_get/) regarding vlans and the veb(4) interface in OpenBSD. This was followed by a [fix](https://marc.info/?l=openbsd-cvs&m=164073300026138&w=2) from dlg@

The issue was similar to something I had previously experimented with using the bridge(4) interface and later veb(4), which I believe is based on the bridge(4) code. I never thought of this as a bug since I think both bridge and veb was working as described in the documentation. Nevertheless I am happy that this fix was committed.

Anyway, what the commit solves is the following...

## Routing 802.1q vlans in OpenBSD

I have two small vlan capable dlink switches and I have an edgerouter lite running OpenBSD. I wanted to connect a vlan trunk from each switch to interfaces on the router.

OpenBSD has had the vlan(4) interface for a long time and a limitation with it is that each vlan interface can only have one parent interface on which it receives the 802.1q packets and tag/untag the vlan id it is interested in. Which means you are only able to send and receive a specific vlan id on one interface. In my case this means the same vlan id cannot exist on both my switches.

I've seen comments and questions about this floating around the internet. Since it differs from e.g. a cisco switch where the same vlan id can belong to and be sent in and out on any of the ports you have on the switch.

A solution now that this fix is in, is using the veb(4) interface to bridge the two vlan trunks on the router, and then add a vport interface to the bridge to act as a parent interface to my vlans.

This is what the configuration could look like on my edgerouter lite, the cnmac interfaces are connected to each switch.

### /etc/hostname.cnmac1

	description "switch1 vlan trunk"
	up

### /etc/hostname.cnmac2

	description "switch2 vlan trunk"
	up

### /etc/hostname.vport0

	description "router vlan trunk"
	up

### /etc/hostname.veb0

	description "trunk bridge"
	link0
	add cnmac1
	add cnmac2
	add vport0
	up

### /etc/hostname.vlan100
	description "management network"
	vnetid 100 parent vport0
	inet 172.16.0.1 255.255.255.0
	up

### /etc/hostname.vlan200
	description "server network"
	vnetid 200 parent vport0
	inet 172.16.10.1 255.255.255.0
	up

### /etc/hostname.vlan300
	description "client network"
	vnetid 300 parent vport0
	inet 172.16.20.1 255.255.255.0
	up

I tested this on OpenBSD 7.1-beta and it seems to work just as expected. My router is now able to route vlans from both my switches and I can use pf to block or allow any packets between my vlans.
