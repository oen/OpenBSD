# Switching between light and dark theme in a tmux pane

Since version 3.3 there is an option in tmux called pane-colours, which lets you set the default colour palette of a pane. Description from the manual:

	pane-colours[] colour
		The default colour palette.  Each entry in the array defines the
		colour tmux uses when the colour with that index is requested.
		The index may be from zero to 255.

Since tmux also accept the notation **colour0** to **colour255** to select colours we can use this to switch the colours around in the default palette for a pane.

In the example below I will use this to switch between dark and light background (like a fancy reverse video). I will use the solarized colours in the example since it's a well known theme with a light and dark mode. Here's the definitions in my .Xresources

	XTerm*color0:   rgb:07/36/42
	XTerm*color1:   rgb:dc/32/2f
	XTerm*color2:   rgb:85/99/00
	XTerm*color3:   rgb:b5/89/00
	XTerm*color4:   rgb:26/8b/d2
	XTerm*color5:   rgb:d3/36/82
	XTerm*color6:   rgb:2a/a1/98
	XTerm*color7:   rgb:ee/e8/d5
	XTerm*color8:   rgb:00/2b/36
	XTerm*color9:   rgb:cb/4b/16
	XTerm*color10:  rgb:58/6e/75
	XTerm*color11:  rgb:65/7b/83
	XTerm*color12:  rgb:83/94/96
	XTerm*color13:  rgb:6c/71/c4
	XTerm*color14:  rgb:93/a1/a1
	XTerm*color15:  rgb:fd/f6/e3

My xterm is configured to use the solarized dark colours for first 16 colours, but that doesn't really matter. Since we are "moving" the colours around to set our reverse palette, I could have used the light mode as my default.

Lets say this is my default theme with dark mode

	colour0,colour1,colour2,colour3,colour4,colour5,colour6,colour7,colour8,colour9,colour10,colour11,colour12,colour13,colour14,colour15

We switch ten colours around to create the "reverse video" of the solarized palette.

	colour7,colour1,colour2,colour3,colour4,colour5,colour6,colour0,colour15,colour9,colour14,colour12,colour11,colour13,colour10,colour8

So lets define a reverse video array in tmux

	%hidden REVERSEVIDEO="colour7,colour1,colour2,colour3,colour4,colour5,colour6,colour0,\
	colour15,colour9,colour14,colour12,colour11,colour13,colour10,colour8"

Then a command named *reverse-video* that sets this palette and a key binding to execute the command

	set-option -s command-alias[10] reverse-video='if-shell -F "#{?pane-colours[0],yes,}" \
	{ set-option -Up pane-colours } \
	{ set-option -op pane-colours $REVERSEVIDEO }'

	bind-key -T prefix v reverse-video

I won't go into the details of the command. Basically it checks if pane-colours are currently set and unsets them, if they are not set it sets pane-colours to the reverse palette array. So the command will toggle the setting for reverse background when you run it.

We bind this new command to the key *v* and now you are able to toggle between light and dark mode by pressing *ctrl-b v*.
